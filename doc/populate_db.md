# Populating the DB with some resources

For testing purpose, you can populate the DB with some resources (e.g generate a
user and associate a lab, some commutes). To see the new commands available you
can launch:

```bash
cd homefs
python manage.py --help
```
