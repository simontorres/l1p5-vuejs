import copy

from rest_framework.response import Response
from rest_framework.permissions import AllowAny
from rest_framework.generics import CreateAPIView
from .serializers import L1P5UserRegistrationSerializer
from rest_framework import status
from rest_framework.decorators import api_view, permission_classes
from rest_framework_simplejwt.views import TokenObtainPairView
from rest_framework import generics, status
from django.contrib.sites.shortcuts import get_current_site
from django.utils.http import urlsafe_base64_encode
from django.contrib.auth.tokens import default_token_generator
from django.utils.http import urlsafe_base64_decode
from django.template.loader import render_to_string
from django.utils.encoding import force_bytes
from django.core.mail import EmailMessage
from django.shortcuts import render

from .models import L1P5User
from .serializers import L1P5TokenObtainPairSerializer, PasswordResetSerializer


class PasswordResetView(generics.GenericAPIView):
    """
    Calls Django Auth PasswordResetForm save method.
    Accepts the following POST parameters: email
    Returns the success/fail message.
    """
    serializer_class = PasswordResetSerializer
    permission_classes = (AllowAny,)
    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(
            {"detail": "L'email a été envoyé."},
            status=status.HTTP_200_OK
        )

class RegisterView(CreateAPIView):
    serializer_class = L1P5UserRegistrationSerializer
    permission_classes = (AllowAny,)
    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            final_user = copy.deepcopy(self.request.data)
            final_user["is_active"] = False
            user = serializer.save(final_user)
            data = {'email': self.request.data['email'],
                    'password': self.request.data['password1']}
            headers = self.get_success_headers(serializer.data)
            mail_subject = 'Activation compte Labos 1point5'
            current_site = get_current_site(request)
            message = render_to_string('registration/acc_active_email.html', {
                'domain': current_site.domain,
                'uid': urlsafe_base64_encode(force_bytes(user.id)),
                'user': user,
                'token': default_token_generator.make_token(user),
            })
            email = EmailMessage(mail_subject, message, to=[user.email])
            email.send()
            return Response(status=status.HTTP_201_CREATED,
                            headers=headers)
        else:
            return Response(serializer.errors,
                            status=status.HTTP_400_BAD_REQUEST)

class L1P5TokenObtainPairView(TokenObtainPairView):
    serializer_class = L1P5TokenObtainPairSerializer

def activate_account(request, uidb64, token):
    """
    View to activate account
    """
    validlink = False
    try:
        uid = urlsafe_base64_decode(uidb64).decode()
        user = L1P5User.objects.get(id=uid)
    except(TypeError, ValueError, OverflowError, L1P5User.DoesNotExist):
        user = None
    if user is not None and default_token_generator.check_token(user, token):
        validlink = True
        user.is_active = True
        user.save()
    return render(request,
                  'registration/acc_activated.html',
                  {'validlink': validlink})

@api_view(['POST'])
@permission_classes([])
def user_exists(request):
    if L1P5User.objects.filter(email=request.data['email']).exists():
        return Response({'exists': True}, status=status.HTTP_200_OK)
    else:
        return Response({'exists': False}, status=status.HTTP_200_OK)

@api_view(['GET'])
@permission_classes([])
def is_super_user(request):
    if request.user.is_authenticated:
        user = L1P5User.objects.get(email=request.user)
        return Response({"is_super_user": user.is_superuser})
    return Response(L1P5User.objects.none())
