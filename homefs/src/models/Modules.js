/**********************************************************************************************************
* Author :
*   Jerome Mariette, INRAE, UR875 Mathématiques et Informatique Appliquées Toulouse, F-31326 Castanet-Tolosan, France
*
* Copyright (C) 2020
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
***********************************************************************************************************/

import Building from '@/models/carbon/Building'
import Commute from '@/models/carbon/Commute'
import ComputerDevice from '@/models/carbon/ComputerDevice'
import Purchase from '@/models/carbon/Purchase'
import Travel from '@/models/carbon/Travel'
import Vehicle from '@/models/carbon/Vehicle'

export default class Modules {
  // Modules definitions
  static get BUILDINGS () { return 'buildings' }
  static get HEATINGS () { return 'heatings' }
  static get ELECTRICITY () { return 'electricity' }
  static get REFRIGERANTS () { return 'refrigerants' }
  static get PURCHASES () { return 'purchases' }
  static get DEVICES () { return 'devices' }
  static get VEHICLES () { return 'vehicles' }
  static get TRAVELS () { return 'travels' }
  static get COMMUTES () { return 'commutes' }

  // Modules colors definitions
  static get HEATING_COLOR () { return '#c84896' }
  static get ELECTRICITY_COLOR () { return '#e88bc3' }
  static get REFRIGERANTS_COLOR () { return '#fdc2e5' }
  static get VEHICLES_COLOR () { return '#fd8e62' }
  static get TRAVELS_COLOR () { return '#67c3a5' }
  static get COMMUTES_COLOR () { return '#8ea0cc' }
  static get DEVICES_COLOR () { return '#a6d953' }
  static get PURCHASES_COLOR () { return '#ffda2c' }

  // Modules icons definitions
  static get BUILDINGS_ICON () { return 'building' }
  static get VEHICLES_ICON () { return 'car' }
  static get TRAVELS_ICON () { return 'train' }
  static get COMMUTES_ICON () { return 'bicycle' }
  static get DEVICES_ICON () { return 'desktop' }
  static get PURCHASES_ICON () { return 'eur' }

  static get MODULES () {
    return {
      [Modules.BUILDINGS]: [
        Modules.HEATINGS,
        Modules.ELECTRICITY,
        Modules.REFRIGERANTS
      ],
      [Modules.PURCHASES]: [],
      [Modules.DEVICES]: [],
      [Modules.VEHICLES]: [],
      [Modules.TRAVELS]: [],
      [Modules.COMMUTES]: []
    }
  }

  static get COLORS () {
    return {
      [Modules.HEATINGS]: Modules.HEATING_COLOR,
      [Modules.ELECTRICITY]: Modules.ELECTRICITY_COLOR,
      [Modules.REFRIGERANTS]: Modules.REFRIGERANTS_COLOR,
      [Modules.PURCHASES]: Modules.PURCHASES_COLOR,
      [Modules.DEVICES]: Modules.DEVICES_COLOR,
      [Modules.VEHICLES]: Modules.VEHICLES_COLOR,
      [Modules.TRAVELS]: Modules.TRAVELS_COLOR,
      [Modules.COMMUTES]: Modules.COMMUTES_COLOR
    }
  }

  static getClasse (module) {
    return {
      [Modules.BUILDINGS]: Building,
      [Modules.PURCHASES]: Purchase,
      [Modules.DEVICES]: ComputerDevice,
      [Modules.VEHICLES]: Vehicle,
      [Modules.TRAVELS]: Travel,
      [Modules.COMMUTES]: Commute
    }[module]
  }

  static getIcon (module) {
    return {
      [Modules.HEATINGS]: Modules.BUILDINGS_ICON,
      [Modules.ELECTRICITY]: Modules.BUILDINGS_ICON,
      [Modules.REFRIGERANTS]: Modules.BUILDINGS_ICON,
      [Modules.BUILDINGS]: Modules.BUILDINGS_ICON,
      [Modules.PURCHASES]: Modules.PURCHASES_ICON,
      [Modules.DEVICES]: Modules.DEVICES_ICON,
      [Modules.VEHICLES]: Modules.VEHICLES_ICON,
      [Modules.TRAVELS]: Modules.TRAVELS_ICON,
      [Modules.COMMUTES]: Modules.COMMUTES_ICON
    }[module]
  }

  static getParentModule (module) {
    return {
      [Modules.HEATINGS]: Modules.BUILDINGS,
      [Modules.ELECTRICITY]: Modules.BUILDINGS,
      [Modules.REFRIGERANTS]: Modules.BUILDINGS,
      [Modules.BUILDINGS]: Modules.BUILDINGS,
      [Modules.PURCHASES]: Modules.PURCHASES,
      [Modules.DEVICES]: Modules.DEVICES,
      [Modules.VEHICLES]: Modules.VEHICLES,
      [Modules.TRAVELS]: Modules.TRAVELS,
      [Modules.COMMUTES]: Modules.COMMUTES
    }[module]
  }

  static getModules (includesSubModules = false, modules = null) {
    let modulesToReturn = []
    if (includesSubModules) {
      if (modules) {
        for (let module of modules) {
          if (Modules.MODULES[module].length > 0) {
            modulesToReturn = modulesToReturn.concat(Modules.MODULES[module])
          } else {
            modulesToReturn.push(module)
          }
        }
      } else {
        for (let module of Object.keys(Modules.MODULES)) {
          if (Modules.MODULES[module].length > 0) {
            modulesToReturn = modulesToReturn.concat(Modules.MODULES[module])
          } else {
            modulesToReturn.push(module)
          }
        }
      }
    } else {
      if (modules) {
        modulesToReturn = modules
      } else {
        modulesToReturn = Object.keys(Modules.MODULES)
      }
    }
    return modulesToReturn
  }
}
