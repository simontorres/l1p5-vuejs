/**********************************************************************************************************
* Author :
*   Jerome Mariette, INRAE, UR875 Mathématiques et Informatique Appliquées Toulouse, F-31326 Castanet-Tolosan, France
*
* Copyright (C) 2020
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
***********************************************************************************************************/

import {
  CarbonIntensities,
  DetailedCarbonIntensity
} from '@/models/carbon/CarbonIntensity.js'
import VEHICLES_FACTORS from '@/../data/vehiclesFactors.json'
import TRANSPORTS_FACTORS from '@/../data/transportsFactors.json'
import EmissionFactor from '@/models/carbon/EmissionFactor.js'

const TRAVELS_MODES = [
  'walking',
  'bike',
  'ebike',
  'escooter',
  'motorbike',
  'car',
  'bus',
  'busintercity',
  'tram',
  'train',
  'expressrailway',
  'subway'
]

const MAX_DISTANCES = {
  'walking': 60,
  'bike': 200,
  'ebike': 200,
  'escooter': 200,
  'motorbike': 2000,
  'car': 2000,
  'bus': 2000,
  'busintercity': 2000,
  'tram': 300,
  'train': 2000,
  'expressrailway': 2000,
  'subway': 250
}

const ICONS = {
  'walking': 'walk',
  'bike': 'bicycle',
  'ebike': 'electric-bike',
  'escooter': 'electric-scooter',
  'motorbike': 'motorcycle',
  'car': 'car',
  'bus': 'bus',
  'busintercity': 'bus',
  'tram': 'tram',
  'train': 'train',
  'expressrailway': 'rer',
  'subway': 'subway'
}

const ICONS_PACK = {
  'walking': 'icomoon',
  'bike': 'sui',
  'ebike': 'icomoon',
  'escooter': 'icomoon',
  'motorbike': 'icomoon',
  'car': 'sui',
  'bus': 'sui',
  'busintercity': 'sui',
  'tram': 'icomoon',
  'train': 'sui',
  'expressrailway': 'icomoon',
  'subway': 'icomoon'
}

const POSITION_RESEARCHER = 'researcher'
const POSITION_ENGINEER = 'engineer'
const POSITION_STUDENT = 'student'
const POSITIONS = [
  POSITION_RESEARCHER,
  POSITION_ENGINEER,
  POSITION_STUDENT
]

export default class Commute {
  constructor (seqID, position, nWorkingDay, message, walking, bike, ebike, escooter,
    motorbike, car, bus, busintercity, tram, train, expressrailway, subway, motorbikepooling, carpooling, engine,
    hasWorkingDay2, nWorkingDay2, walking2, bike2, ebike2, escooter2, motorbike2, car2,
    bus2, busintercity2, tram2, train2, expressrailway2, subway2, motorbikepooling2, carpooling2, engine2, deleted) {
    this.seqID = seqID
    this.position = position
    this.nWorkingDay = nWorkingDay
    this.message = message

    this.walking = walking
    this.bike = bike
    this.ebike = ebike
    this.escooter = escooter
    this.motorbike = motorbike
    this.car = car
    this.bus = bus
    this.busintercity = busintercity
    this.tram = tram
    this.train = train
    this.expressrailway = expressrailway
    this.subway = subway
    this.motorbikepooling = motorbikepooling
    this.carpooling = carpooling
    this.engine = engine

    this.hasWorkingDay2 = hasWorkingDay2
    this.nWorkingDay2 = nWorkingDay2
    this.walking2 = walking2
    this.bike2 = bike2
    this.ebike2 = ebike2
    this.escooter2 = escooter2
    this.motorbike2 = motorbike2
    this.car2 = car2
    this.bus2 = bus2
    this.busintercity2 = busintercity2
    this.tram2 = tram2
    this.train2 = train2
    this.expressrailway2 = expressrailway2
    this.subway2 = subway2
    this.motorbikepooling2 = motorbikepooling2
    this.carpooling2 = carpooling2
    this.engine2 = engine2

    this.deleted = deleted
    this.isValid = true
  }

  toString (sep = '\t') {
    return [
      this.seqID,
      this.deleted,
      this.nWorkingDay,
      this.position,
      this.engine,
      this.motorbikepooling,
      this.carpooling,
      this.getRawDistance1().map(val => Math.round(val)).join(sep),
      this.nWorkingDay2,
      this.engine2,
      this.motorbikepooling2,
      this.carpooling2,
      this.getRawDistance2().map(val => Math.round(val)).join(sep),
      this.intensities.map(obj => Math.round(obj.intensity)).join(sep),
      Math.round(this.intensities.map(obj => obj.intensity).reduce(function (a, b) {
        return a + b
      }, 0))
    ].join(sep)
  }

  getNumberOfWorkedWeeks (year, settings) {
    let nbWeeks = settings.filter(obj => obj.name === 'NUMBER_OF_WORKED_WEEK.default')[0].value
    let years = settings.map(a => a.name.slice(-4))
    year = year.toString()
    if (years.includes(year)) {
      nbWeeks = settings.filter(obj => obj.name.endsWith(year))[0].value
    }
    return nbWeeks
  }

  isCarpooling () {
    let isCarpooling = false
    if (this.car > 0 && this.carpooling > 1) {
      isCarpooling = true
    } else if (this.car2 > 0 && this.carpooling2 > 1) {
      isCarpooling = true
    } else if (this.motorbike > 0 && this.motorbikepooling > 1) {
      isCarpooling = true
    } else if (this.motorbike2 > 0 && this.motorbikepooling2 > 1) {
      isCarpooling = true
    }
    return isCarpooling
  }

  getNbDayType1 () {
    let nbDayType1 = this.nWorkingDay
    if (this.hasWorkingDay2) {
      nbDayType1 = this.nWorkingDay - this.nWorkingDay2
    }
    return nbDayType1
  }

  /**
   * Get number of days per mode
   */
  getCDays () {
    let cdays = Array(Commute.travelsModes.length).fill(0)
    let nbDayType1 = this.getNbDayType1()
    let index = 0
    for (let mode of Commute.travelsModes) {
      if (this[mode] !== 0 && !isNaN(this[mode])) {
        cdays[index] += nbDayType1
      }
      if (this.hasWorkingDay2) {
        if (this[mode + '2'] !== 0 && !isNaN(this[mode + '2'])) {
          cdays[index] += this.nWorkingDay2
        }
      }
      index += 1
    }
    return cdays
  }

  getRawDistance1 () {
    let total = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    let index = 0
    for (let mode of TRAVELS_MODES) {
      if (this[mode] !== 0 && !isNaN(this[mode])) {
        total[index] += parseFloat(this[mode])
      }
      index += 1
    }
    return total
  }

  getRawDistance2 () {
    // let self = this
    // TRAVELS_MODES.map(key => self[key])
    let total = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    let index = 0
    for (let mode of TRAVELS_MODES) {
      if (this.hasWorkingDay2) {
        if (this[mode + '2'] !== 0 && !isNaN(this[mode + '2'])) {
          total[index] += parseFloat(this[mode + '2'])
        }
      }
      index += 1
    }
    return total
  }

  getRawDistance (GHGIYear, settings) {
    let total = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    let index = 0
    const nbWeeks = this.getNumberOfWorkedWeeks(GHGIYear, settings)
    let faAnnuel1 = this.nWorkingDay * nbWeeks
    if (this.hasWorkingDay2) {
      faAnnuel1 = (this.nWorkingDay - this.nWorkingDay2) * nbWeeks
    }
    for (let mode of TRAVELS_MODES) {
      total[index] += this[mode] * faAnnuel1
      if (this.hasWorkingDay2) {
        let faAnnuel2 = this.nWorkingDay2 * nbWeeks
        total[index] += this[mode + '2'] * faAnnuel2
      }
      index += 1
    }
    return total
  }

  getRawWeekDistance1 () {
    let total = 0
    for (let mode of TRAVELS_MODES) {
      if (this[mode] !== 0 && !isNaN(this[mode])) {
        total += parseFloat(this[mode])
      }
    }
    if (this.hasWorkingDay2) {
      total = total * (this.nWorkingDay - this.nWorkingDay2)
    } else {
      total = total * this.nWorkingDay
    }
    return total
  }

  getRawWeekDistance2 () {
    let total = 0
    if (this.hasWorkingDay2) {
      for (let mode of TRAVELS_MODES) {
        if (this[mode + '2'] !== 0 && !isNaN(this[mode + '2'])) {
          total += parseFloat(this[mode + '2'])
        }
      }
    }
    return total * this.nWorkingDay2
  }

  getRawWeekDistance () {
    return this.getRawWeekDistance1() + this.getRawWeekDistance2()
  }

  getDistanceTotale (settings, GHGIYear = null) {
    return this.getRawDistance(GHGIYear, settings).reduce((a, b) => a + b)
  }

  getTotalIntensity (settings, GHGIYear = null, citySize = 0) {
    let intensities = new CarbonIntensities()
    for (let intensity of this.getCarbonIntensity(citySize, settings, GHGIYear)) {
      intensities.add(intensity)
    }
    return intensities.sum()
  }

  getEmissionFactor (mode, engine, citySize = null, year = null) {
    let ef = EmissionFactor.createFromObj()
    if (mode === 'walking') {
      ef.group = 'walking'
    } else if (mode === 'bike') {
      ef = EmissionFactor.createFromObj(VEHICLES_FACTORS['bike']['muscular'])
    } else if (mode === 'ebike') {
      ef = EmissionFactor.createFromObj(VEHICLES_FACTORS['bike']['electric'])
    } else if (mode === 'escooter') {
      ef = EmissionFactor.createFromObj(VEHICLES_FACTORS['scooter']['electric'])
    } else if (mode === 'motorbike') {
      ef = EmissionFactor.createFromObj(VEHICLES_FACTORS['motorbike']['gasoline'])
    } else if (mode === 'car') {
      ef = EmissionFactor.createFromObj(VEHICLES_FACTORS['car'][engine])
    } else if (mode === 'bus') {
      if (citySize === 0) {
        ef = EmissionFactor.createFromObj(TRANSPORTS_FACTORS['bus']['bus.smallcity'])
      } else if (citySize === 1) {
        ef = EmissionFactor.createFromObj(TRANSPORTS_FACTORS['bus']['bus.mediumcity'])
      } else {
        ef = EmissionFactor.createFromObj(TRANSPORTS_FACTORS['bus']['bus.bigcity'])
      }
    } else if (mode === 'busintercity') {
      ef = EmissionFactor.createFromObj(TRANSPORTS_FACTORS['bus']['bus.intercity'])
    } else if (mode === 'train') {
      // The survey asks for dailyDistance, to choose the emission
      // factor, we considere its go and back
      let commuteDistance = this[mode] / 2
      if (commuteDistance < 200) {
        ef = EmissionFactor.createFromObj(TRANSPORTS_FACTORS['railway']['train.shortdistance'])
      } else {
        ef = EmissionFactor.createFromObj(TRANSPORTS_FACTORS['railway']['train.longdistance'])
      }
    } else if (mode === 'tram') {
      if (citySize === 0) {
        ef = EmissionFactor.createFromObj(TRANSPORTS_FACTORS['railway']['tram.mediumcity'])
      } else if (citySize === 1) {
        ef = EmissionFactor.createFromObj(TRANSPORTS_FACTORS['railway']['tram.mediumcity'])
      } else {
        ef = EmissionFactor.createFromObj(TRANSPORTS_FACTORS['railway']['tram.bigcity'])
      }
    } else if (mode === 'expressrailway') {
      ef = EmissionFactor.createFromObj(TRANSPORTS_FACTORS['railway']['rer'])
    } else if (mode === 'subway') {
      ef = EmissionFactor.createFromObj(TRANSPORTS_FACTORS['railway']['subway'])
    }
    return ef.getFactor(year)
  }

  getCarbonIntensity (citySize, settings, GHGIYear = null) {
    let intensities = new Array(Commute.travelsModes.length).fill(0)
    const nbWeeks = this.getNumberOfWorkedWeeks(GHGIYear, settings)
    for (let i in intensities) {
      intensities[i] = new CarbonIntensities()
    }
    let index = 0
    if (!this.deleted) {
      for (let mode of TRAVELS_MODES) {
        if (this[mode] !== 0 && !isNaN(this[mode])) {
          let ef = this.getEmissionFactor(mode, this.engine, citySize, GHGIYear)
          let distance = this[mode]
          if (mode === 'car') {
            distance = distance / this.carpooling
          } else if (mode === 'motorbike') {
            distance = distance / this.motorbikepooling
          }
          let faAnnuel = this.nWorkingDay * nbWeeks
          if (this.hasWorkingDay2) {
            faAnnuel = (this.nWorkingDay - this.nWorkingDay2) * nbWeeks
          }
          intensities[index].add(new DetailedCarbonIntensity(
            0.0,
            0.0,
            0.0,
            0.0,
            distance * faAnnuel * ef.combustion.total,
            distance * faAnnuel * ef.combustion.total * ef.combustion.uncertainty,
            distance * faAnnuel * ef.upstream.total,
            distance * faAnnuel * ef.upstream.total * ef.upstream.uncertainty,
            distance * faAnnuel * ef.manufacturing.total,
            distance * faAnnuel * ef.manufacturing.total * ef.manufacturing.uncertainty,
            ef.group
          ))
        }
        if (this.hasWorkingDay2) {
          if (this[mode + '2'] !== 0 && !isNaN(this[mode + '2'])) {
            let ef2 = this.getEmissionFactor(mode, this.engine2, citySize, GHGIYear)
            let distance2 = this[mode + '2']
            if (mode === 'car') {
              distance2 = distance2 / this.carpooling2
            } else if (mode === 'motorbike') {
              distance2 = distance2 / this.motorbikepooling2
            }
            let faAnnuel2 = this.nWorkingDay2 * nbWeeks
            intensities[index].add(new DetailedCarbonIntensity(
              0.0,
              0.0,
              0.0,
              0.0,
              distance2 * faAnnuel2 * ef2.combustion.total,
              distance2 * faAnnuel2 * ef2.combustion.total * ef2.combustion.uncertainty,
              distance2 * faAnnuel2 * ef2.upstream.total,
              distance2 * faAnnuel2 * ef2.upstream.total * ef2.upstream.uncertainty,
              distance2 * faAnnuel2 * ef2.manufacturing.total,
              distance2 * faAnnuel2 * ef2.manufacturing.total * ef2.manufacturing.uncertainty,
              ef2.group
            ))
          }
        }
        index += 1
      }
    }
    // to correctly compute the uncertainty propagation, returns
    // CarbonIntensities instead of CarbonIntensity object
    // return intensities.map(obj => obj.sum())
    this.intensities = intensities
    return intensities
  }

  toDatabase () {
    let nWorkingDay2 = 0
    if (this.nWorkingDay2 !== null) {
      nWorkingDay2 = this.nWorkingDay2
    }
    return {
      'seqID': this.seqID,
      'position': this.position,
      'nWorkingDay': this.nWorkingDay,
      'message': this.message,
      'walking': this.walking,
      'bike': this.bike,
      'ebike': this.ebike,
      'escooter': this.escooter,
      'motorbike': this.motorbike,
      'car': this.car,
      'bus': this.bus,
      'busintercity': this.busintercity,
      'tram': this.tram,
      'train': this.train,
      'expressrailway': this.expressrailway,
      'subway': this.subway,
      'motorbikepooling': this.motorbikepooling,
      'carpooling': this.carpooling,
      'engine': this.engine,
      'hasWorkingDay2': this.hasWorkingDay2,
      'nWorkingDay2': nWorkingDay2,
      'walking2': this.walking2,
      'bike2': this.bike2,
      'ebike2': this.ebike2,
      'escooter2': this.escooter2,
      'motorbike2': this.motorbike2,
      'car2': this.car2,
      'bus2': this.bus2,
      'busintercity2': this.busintercity2,
      'tram2': this.tram2,
      'train2': this.train2,
      'expressrailway2': this.expressrailway2,
      'subway2': this.subway2,
      'motorbikepooling2': this.motorbikepooling2,
      'carpooling2': this.carpooling2,
      'engine2': this.engine2,
      'deleted': this.deleted
    }
  }

  static get travelsModes () {
    return TRAVELS_MODES
  }

  static getIcon (mode) {
    return ICONS[mode]
  }

  static getIconPack (mode) {
    return ICONS_PACK[mode]
  }

  static getMaxDistance (mode) {
    return MAX_DISTANCES[mode]
  }

  static createFromObj (commute) {
    return new Commute(
      commute.seqID,
      commute.position,
      commute.nWorkingDay,
      commute.message,
      commute.walking,
      commute.bike,
      commute.ebike,
      commute.escooter,
      commute.motorbike,
      commute.car,
      commute.bus,
      commute.busintercity,
      commute.tram,
      commute.train,
      commute.expressrailway,
      commute.subway,
      commute.motorbikepooling,
      commute.carpooling,
      commute.engine,
      commute.hasWorkingDay2,
      commute.nWorkingDay2,
      commute.walking2,
      commute.bike2,
      commute.ebike2,
      commute.escooter2,
      commute.motorbike2,
      commute.car2,
      commute.bus2,
      commute.busintercity2,
      commute.tram2,
      commute.train2,
      commute.expressrailway2,
      commute.subway2,
      commute.motorbikepooling2,
      commute.carpooling2,
      commute.engine2,
      commute.deleted
    )
  }

  static initiStatusTable (length, fillIntensities = false) {
    let table = {}
    for (let position of POSITIONS) {
      table[position] = new Array(length).fill(0)
    }
    if (fillIntensities) {
      for (let key of Object.keys(table)) {
        for (let i in table[key]) {
          table[key][i] = new CarbonIntensities()
        }
      }
      return table
    } else {
      return table
    }
  }

  static get POSITIONS () {
    return POSITIONS
  }

  static get POSITION_RESEARCHER () {
    return POSITION_RESEARCHER
  }

  static get POSITION_ENGINEER () {
    return POSITION_ENGINEER
  }

  static get POSITION_STUDENT () {
    return POSITION_STUDENT
  }

  static exportHeader (sep = '\t') {
    return [
      'seqid',
      'deleted',
      'commuting.days',
      'position',
      'engine.std1',
      'motorbikepooling.std1',
      'carpooling.std1',
      'walking.std1.km',
      'bike.std1.km',
      'ebike.std1.km',
      'escooter.std1.km',
      '2wheeler.std1.km',
      'car.std1.km',
      'bus.std1.km',
      'busintercity.std1.km',
      'tramway.std1.km',
      'train.std1.km',
      'rer.std1.km',
      'subway.std1.km',
      'commuting.std2.days',
      'engine.std2',
      'motorbikepooling.std2',
      'carpooling.std2',
      'walking.std2.km',
      'bike.std2.km',
      'ebike.std2.km',
      'escooter.std2.km',
      '2wheeler.std2.km',
      'car.std2.km',
      'bus.std2.km',
      'busintercity.std2.km',
      'tramway.std2.km',
      'train.std2.km',
      'rer.std2.km',
      'subway.std2.km',
      'walking.kg.co2e',
      'bike.kg.co2e',
      'ebike.kg.co2e',
      'escooter.kg.co2e',
      '2wheeler.kg.co2e',
      'car.kg.co2e',
      'bus.kg.co2e',
      'busintercity.kg.co2e',
      'tramway.kg.co2e',
      'train.kg.co2e',
      'rer.kg.co2e',
      'subway.kg.co2e',
      'total.kg.co2e'
    ].join(sep)
  }

  static exportToFile (items, header = true, extraColValue = null, sep = '\t') {
    let headerValues = []
    if (header) {
      headerValues = Commute.exportHeader(sep)
    }
    return [
      headerValues,
      ...items.map(function (item) {
        let val = ''
        if (extraColValue) {
          val += extraColValue + sep
        }
        return val + item.toString(sep)
      })
    ]
      .join('\n')
      .replace(/(^\[)|(\]$)/gm, '')
  }

  static okToSubmit (items) {
    return items.length > 0
  }

  static compute (
    commutes,
    citySize,
    nresearcher,
    nteacher,
    nengineer,
    nstudent,
    ghgiYear,
    settings
  ) {
    let intensities = Commute.initiStatusTable(Commute.travelsModes.length, true)
    let distances = Commute.initiStatusTable(Commute.travelsModes.length)
    // hold the cumulative number of day each travel mode is used indexed by the position
    let volumePerMode = Commute.initiStatusTable(Commute.travelsModes.length)
    let cdays = {}
    let nbAnswers = {}
    for (let position of Commute.POSITIONS) {
      cdays[position] = 0
      nbAnswers[position] = 0
    }
    let totalPerCommute = []
    for (let commute of commutes) {
      let intensity = commute.getCarbonIntensity(citySize, settings, ghgiYear)
      if (!commute.deleted) {
        let cDistances = commute.getRawDistance(ghgiYear, settings)
        let nDaysPermode = commute.getCDays()
        totalPerCommute.unshift([commute.seqID].concat(intensity))
        nbAnswers[commute.position] += 1
        cdays[commute.position] += commute.nWorkingDay * commute.getNumberOfWorkedWeeks(ghgiYear, settings)
        for (let index = 0; index < Commute.travelsModes.length; index++) {
          intensities[commute.position][index].add(intensity[index])
          distances[commute.position][index] += cDistances[index]
          volumePerMode[commute.position][index] += nDaysPermode[index] * commute.getNumberOfWorkedWeeks(ghgiYear, settings)
        }
      }
    }
    // convert CarbonIntensities to CarbonIntensity for normalization
    Object.keys(intensities).map(function (key) {
      intensities[key] = intensities[key].map(obj => obj.sum())
    })
    // normalize intensity by the number of lab members
    for (let position of Object.keys(intensities)) {
      for (let index = 0; index < Commute.travelsModes.length; index++) {
        let normalizationFactor = 0
        if (nbAnswers[position] !== 0 && position === Commute.POSITION_RESEARCHER) {
          normalizationFactor = (nresearcher + nteacher) / nbAnswers[position]
        } else if (nbAnswers[position] !== 0 && position === Commute.POSITION_ENGINEER) {
          normalizationFactor = nengineer / nbAnswers[position]
        } else if (nbAnswers[position] !== 0 && position === Commute.POSITION_STUDENT) {
          normalizationFactor = nstudent / nbAnswers[position]
        }
        intensities[position][index] = intensities[position][index].multiply(normalizationFactor)
        distances[position][index] = distances[position][index] * normalizationFactor
        volumePerMode[position][index] = volumePerMode[position][index] * normalizationFactor
      }
    }
    let total = new CarbonIntensities()
    for (let position of Object.keys(intensities)) {
      for (let index = 0; index < Commute.travelsModes.length; index++) {
        total.add(intensities[position][index])
      }
    }
    return {
      'intensity': total.sum(),
      'totalPerCommute': totalPerCommute,
      'status': {
        'intensity': intensities,
        'distances': distances,
        'volumePerMode': volumePerMode
      },
      'nbAnswers': nbAnswers,
      'cdays': cdays
    }
  }
}
