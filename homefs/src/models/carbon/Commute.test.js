import Commute from './Commute.js'
import { DEFAULT_SETTINGS } from '@/stores/constants.js'

/**
 *
 * @param {Object} modifier key, value to modify in the default commute Object
 *
 * @returns {Commute}
 */
function commuteObj (modifier) {
  let commute = {
    'seqID': null,
    'position': null,
    'nWorkingDay': null,
    'message': null,
    'walking': 0,
    'bike': 0,
    'ebike': 0,
    'escooter': 0,
    'motorbike': 0,
    'car': 0,
    'bus': 0,
    'busintercity': 0,
    'tram': 0,
    'train': 0,
    'expressrailway': 0,
    'subway': 0,
    'motorbikepooling': 1,
    'carpooling': 1,
    'hasWorkingDay2': false,
    'nWorkingDay2': null,
    'engine': null,
    'walking2': 0,
    'bike2': 0,
    'ebike2': 0,
    'escooter2': 0,
    'motorbike2': 0,
    'car2': 0,
    'bus2': 0,
    'busintercity2': 0,
    'tram2': 0,
    'train2': 0,
    'expressrailway2': 0,
    'subway2': 0,
    'motorbikepooling2': 1,
    'carpooling2': 1,
    'engine2': null
  }

  if (modifier === undefined) {
    modifier = {}
  }

  for (let [key, value] of Object.entries(modifier)) {
    commute[key] = value
  }
  return Commute.createFromObj(commute)
}

function zerosExcept (modifier) {
  // initialize an array with some value changed
  let result = new Array(Commute.travelsModes.length).fill(0)
  if (modifier === undefined) {
    modifier = {}
  }

  for (let [mode, value] of Object.entries(modifier)) {
    result[Commute.travelsModes.indexOf(mode)] = value
  }
  return result
}

describe('isCarPooling', () => {
  test('car=0, bike=0 should return false', () => {
    let commute = commuteObj()
    expect(commute.isCarpooling()).toBeFalsy()
  })

  // type1 carpooling
  test('car=1, carpooling=2 should return true', () => {
    let commute = commuteObj({ car: 1, carpooling: 2 })
    expect(commute.isCarpooling()).toBeTruthy()
  })

  test('motorbike=1, motorbikepooling=2 should return true', () => {
    let commute = commuteObj({ motorbike: 1, motorbikepooling: 2 })
    expect(commute.isCarpooling()).toBeTruthy()
  })

  // type2  car pooling (suffixed by 2)
  test('car2=1, carpooling2=2 should return true', () => {
    let commute = commuteObj({ car2: 1, carpooling2: 2 })
    expect(commute.isCarpooling()).toBeTruthy()
  })

  test('motorbike2=1, motorbikepooling2=2 should return true', () => {
    let commute = commuteObj({ motorbike2: 1, motorbikepooling2: 2 })
    expect(commute.isCarpooling()).toBeTruthy()
  })
})

describe('compute', () => {
  test('return value with no commutes: check anything but intensity', () => {
    let r = Commute.compute([], 0, 1, 1, 1, 1, 2042, DEFAULT_SETTINGS)
    // console.log(r)
    expect(r).toEqual(expect.objectContaining({
      totalPerCommute: [],
      status: expect.objectContaining({
        distances: {
          researcher: zerosExcept(),
          engineer: zerosExcept(),
          student: zerosExcept()
        } }),
      nbAnswers: { researcher: 0, engineer: 0, student: 0 },
      cdays: { researcher: 0, engineer: 0, student: 0 }
    }))
  })

  test('return value with [{car=1, engine=diesel, nbWorkingDay=5, position=engineer}] -- 100% answers: check anything but intensity', () => {
    let commute = commuteObj({ car: 10, engine: 'diesel', nWorkingDay: 5, position: 'engineer' })

    let r = Commute.compute([commute], 0, 1, 1, 1, 1, 2042, DEFAULT_SETTINGS)
    // console.log(r)
    expect(r).toEqual(expect.objectContaining({
      // TODO
      // totalPerCommute
      // TODO
      // intensity
      status: expect.objectContaining({
        distances: {
          researcher: zerosExcept(),
          engineer: zerosExcept({ car: 10 * 5 * 41 }),
          student: zerosExcept()
        },
        volumePerMode: {
          researcher: zerosExcept(),
          engineer: zerosExcept({ car: 41 * 5 }),
          student: zerosExcept()
        }
      }),
      nbAnswers: { researcher: 0, engineer: 1, student: 0 },
      cdays: { researcher: 0, engineer: 41 * 5 * 1, student: 0 }
    })) // expect
  })

  test('return value with [{car=1, engine=diesel, nbWorkingDay=5, position=engineer}] -- 50% answers: check anything but intensity', () => {
    let commute = commuteObj({ car: 10, engine: 'diesel', nWorkingDay: 5, position: 'engineer' })

    // Consider having 1 answer over 2 engineers (50%)
    let r = Commute.compute([commute], 0, 1, 1, 2, 1, 2042, DEFAULT_SETTINGS)
    // console.log(r)
    expect(r).toEqual(expect.objectContaining({
      // TODO
      // totalPerCommute
      // TODO
      // intensity
      status: expect.objectContaining({
        distances: {
          researcher: zerosExcept(),
          // extrapolate to all the engineer : x2
          engineer: zerosExcept({ car: 2 * 10 * 5 * 41 }),
          student: zerosExcept()
        },
        volumePerMode: {
          researcher: zerosExcept(),
          engineer: zerosExcept({ car: 2 * 5 * 41 }),
          student: zerosExcept()
        }
      }),
      nbAnswers: { researcher: 0, engineer: 1, student: 0 },
      cdays: { researcher: 0, engineer: 41 * 5 * 1, student: 0 }
    })) // expect
  })
})
