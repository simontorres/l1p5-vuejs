/**********************************************************************************************************
* Author :
*   Jerome Mariette, INRAE, UR875 Mathématiques et Informatique Appliquées Toulouse, F-31326 Castanet-Tolosan, France
*
* Copyright (C) 2020
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
***********************************************************************************************************/

import Building from '@/models/carbon/Building'
import Commute from '@/models/carbon/Commute'
import ComputerDevice from '@/models/carbon/ComputerDevice'
import Purchase from '@/models/carbon/Purchase'
import Travel from '@/models/carbon/Travel'
import Vehicle from '@/models/carbon/Vehicle'
import Modules from '@/models/Modules.js'
import {
  BuildingCollection,
  DeviceCollection,
  PurchaseCollection,
  VehicleCollection,
  TravelCollection,
  CommuteCollection
} from '@/models/carbon/Collection.js'
import Synthesis from '@/models/carbon/Synthesis'
import { CarbonIntensities } from '@/models/carbon/CarbonIntensity.js'

export default class GHGI {
  constructor (id = null, uuid = null, year = null, created = null,
    description = null, nResearcher = 0, nProfessor = 0, nEngineer = 0,
    nStudent = 0, budget = 0, surveyMessage = null, surveyActive = false,
    laboratory = null, buildingsSubmitted = false, commutesSubmitted = false,
    travelsSubmitted = false, vehiclesSubmitted = false, devicesSubmitted = false,
    purchasesSubmitted = false, surveyCloneYear = null, buildings = [],
    purchases = [], devices = [], vehicles = [], travels = [], commutes = [],
    synthesis = null, emissions = {}) {
    this.id = id
    this.uuid = uuid
    this.year = year
    this.created = created
    this.description = description
    this.nResearcher = nResearcher
    this.nProfessor = nProfessor
    this.nEngineer = nEngineer
    this.nStudent = nStudent
    this.budget = budget
    this.surveyMessage = surveyMessage
    this.surveyActive = surveyActive
    if (laboratory) {
      this.laboratory = laboratory
    } else {
      this.laboratory = {
        'area': null,
        'country': null,
        'citySize': null
      }
    }
    this.buildingsSubmitted = buildingsSubmitted
    this.commutesSubmitted = commutesSubmitted
    this.travelsSubmitted = travelsSubmitted
    this.vehiclesSubmitted = vehiclesSubmitted
    this.devicesSubmitted = devicesSubmitted
    this.purchasesSubmitted = purchasesSubmitted
    this.surveyCloneYear = surveyCloneYear

    // Create collections
    this.buildings = new BuildingCollection()
    this.purchases = new PurchaseCollection()
    this.devices = new DeviceCollection()
    this.vehicles = new VehicleCollection()
    this.travels = new TravelCollection()
    this.commutes = new CommuteCollection()

    // Add objects to collections
    buildings.map((obj) => this.buildings.add(obj))
    purchases.map((obj) => this.purchases.add(obj))
    devices.map((obj) => this.devices.add(obj))
    vehicles.map((obj) => this.vehicles.add(obj))
    travels.map((obj) => this.travels.add(obj))
    commutes.map((obj) => this.commutes.add(obj))

    this.synthesis = Synthesis.createFromObj(synthesis)
    this.emissions = emissions
  }

  get intensity () {
    let intensity = new CarbonIntensities()
    intensity.add(this.emissions.buildings.heatings.intensity)
    intensity.add(this.emissions.buildings.electricity)
    intensity.add(this.emissions.buildings.refrigerants)
    intensity.add(this.emissions.purchases.intensity)
    intensity.add(this.emissions.devices.intensity)
    intensity.add(this.emissions.vehicles.intensity)
    intensity.add(this.emissions.travels.intensity)
    intensity.add(this.emissions.commutes.intensity)
    return intensity.sum()
  }

  get workforce () {
    return this.nResearcher + this.nProfessor + this.nEngineer + this.nStudent
  }

  toString (sep = '\t') {
    return [
      this.uuid,
      this.created,
      this.year,
      this.laboratory.id,
      this.laboratory.administrations.map(obj => obj.name).join(';'),
      this.laboratory.disciplines.map(obj => obj.name + ':' + obj.percentage).join(';'),
      this.laboratory.mainSite,
      this.laboratory.country,
      this.laboratory.area,
      this.nResearcher,
      this.nProfessor,
      this.nEngineer,
      this.nStudent,
      this.budget,
      this.vehiclesSubmitted,
      Math.round(this.synthesis.vehicles.getIntensity()),
      Math.round(this.synthesis.vehicles.getUncertainty()),
      this.travelsSubmitted,
      Math.round(this.synthesis.travels.getIntensity()),
      Math.round(this.synthesis.travels.getUncertainty()),
      Math.round(this.synthesis.travels.getIntensity(true)),
      Math.round(this.synthesis.travels.getUncertainty(true)),
      this.commutesSubmitted,
      Math.round(this.synthesis.commutes.getIntensity()),
      Math.round(this.synthesis.commutes.getUncertainty()),
      this.buildingsSubmitted,
      Math.round(this.synthesis.heatings.getIntensity()),
      Math.round(this.synthesis.heatings.getUncertainty()),
      this.buildingsSubmitted,
      Math.round(this.synthesis.electricity.getIntensity()),
      Math.round(this.synthesis.electricity.getUncertainty()),
      this.buildingsSubmitted,
      Math.round(this.synthesis.refrigerants.getIntensity()),
      Math.round(this.synthesis.refrigerants.getUncertainty()),
      this.devicesSubmitted,
      Math.round(this.synthesis.devices.getIntensity()),
      Math.round(this.synthesis.devices.getUncertainty()),
      this.purchasesSubmitted,
      Math.round(this.synthesis.purchases.getIntensity()),
      Math.round(this.synthesis.purchases.getUncertainty())
    ].join(sep)
  }

  compute (commuteSettings, year = null, module = null) {
    let cyear = this.year
    if (year !== null) {
      cyear = year
    }
    if (module) {
      if ([Modules.Buildings, Modules.HEATINGS, Modules.ELECTRICITY, Modules.REFRIGERANTS].includes(module)) {
        this.emissions.buildings = Building.compute(
          this.buildings,
          this.laboratory.area,
          this.laboratory.country,
          cyear
        )
      } else if (module === Modules.COMMUTES) {
        this.emissions.commutes = Commute.compute(
          this.commutes,
          this.laboratory.citySize,
          this.nResearcher,
          this.nProfessor,
          this.nEngineer,
          this.nStudent,
          cyear,
          commuteSettings
        )
      } else if (module === Modules.PURCHASES) {
        this.emissions.purchases = Purchase.compute(
          this.purchases,
          cyear
        )
      } else if (module === Modules.DEVICES) {
        this.emissions.devices = ComputerDevice.compute(
          this.devices
        )
      } else if (module === Modules.VEHICLES) {
        this.emissions.vehicles = Vehicle.compute(
          this.vehicles,
          cyear
        )
      } else if (module === Modules.TRAVELS) {
        this.emissions.travels = Travel.compute(
          this.travels,
          cyear
        )
      }
    } else {
      this.emissions = {
        buildings: Building.compute(
          this.buildings,
          this.laboratory.area,
          this.laboratory.country,
          cyear
        ),
        commutes: Commute.compute(
          this.commutes,
          this.laboratory.citySize,
          this.nResearcher,
          this.nProfessor,
          this.nEngineer,
          this.nStudent,
          cyear,
          commuteSettings
        ),
        vehicles: Vehicle.compute(
          this.vehicles,
          cyear
        ),
        travels: Travel.compute(
          this.travels,
          cyear
        ),
        devices: ComputerDevice.compute(
          this.devices
        ),
        purchases: Purchase.compute(
          this.purchases,
          cyear
        )
      }
    }

    // Update synthesis
    this.synthesis = new Synthesis(
      this.emissions.buildings.heatings.intensity,
      this.emissions.buildings.electricity.toCarbonIntensity(),
      this.emissions.buildings.refrigerants,
      this.emissions.commutes.intensity,
      this.emissions.travels.intensity,
      this.emissions.vehicles.intensity,
      this.emissions.devices.intensity,
      this.emissions.purchases.intensity
    )
  }

  static exportHeader (sep = '\t') {
    return [
      'ghgi.uuid',
      'created.date',
      'year',
      'lab.id',
      'supervisions',
      'disciplines',
      'city',
      'country',
      'region',
      'no.researchers',
      'no.teacher',
      'no.ita',
      'no.postdoc',
      'budget',
      'vehicles.submitted',
      'vehicles.emission.kg.co2e',
      'vehicles.uncertainty.kg.co2e',
      'travels.submitted',
      'travels.emission.kg.co2e',
      'travels.uncertainty.kg.co2e',
      'travelswc.emission.kg.co2e',
      'travelswc.uncertainty.kg.co2e',
      'commutes.submitted',
      'commutes.emission.kg.co2e',
      'commutes.uncertainty.kg.co2e',
      'heating.submitted',
      'heating.emission.kg.co2e',
      'heating.uncertainty.kg.co2e',
      'electricity.submitted',
      'electricity.emission.kg.co2e',
      'electricity.uncertainty.kg.co2e',
      'cooling.submitted',
      'cooling.emission.kg.co2e',
      'cooling.uncertainty.kg.co2e',
      'devices.submitted',
      'devices.emission.kg.co2e',
      'devices.uncertainty.kg.co2e',
      'purchases.submitted',
      'purchases.emission.kg.co2e',
      'purchases.uncertainty.kg.co2e'
    ].join(sep)
  }

  static exportToFile (items, header = true, extraColValue = null, sep = '\t') {
    let headerValues = []
    if (header) {
      headerValues = GHGI.exportHeader(sep)
    }
    return [
      headerValues,
      ...items.map(function (item) {
        let val = ''
        if (extraColValue) {
          val += extraColValue + sep
        }
        return val + item.toString(sep)
      })
    ]
      .join('\n')
      .replace(/(^\[)|(\]$)/gm, '')
  }

  static createFromObj (item) {
    return new GHGI(
      item.id,
      item.uuid,
      item.year,
      item.created,
      item.description,
      item.nResearcher,
      item.nProfessor,
      item.nEngineer,
      item.nStudent,
      item.budget,
      item.surveyMessage,
      item.surveyActive,
      item.laboratory,
      item.buildingsSubmitted,
      item.commutesSubmitted,
      item.travelsSubmitted,
      item.vehiclesSubmitted,
      item.devicesSubmitted,
      item.purchasesSubmitted,
      item.surveyCloneYear,
      item.buildings,
      item.purchases,
      item.devices,
      item.vehicles,
      item.travels,
      item.commutes,
      Synthesis.createFromObj(item.synthesis),
      item.emissions
    )
  }
}
