import { defineStore } from 'pinia'
import { useCoreStore } from './core'

import carbonService from '@/services/carbonService'
import scenarioService from '@/services/scenarioService'
import transitionService from '@/services/transitionService'
import Scenario from '@/models/scenario/Scenario.js'
import GHGI from '@/models/carbon/GHGI.js'

export const useSuperadminStore = defineStore('superadmin', {
  state: () => ({
    allGHGI: [],
    scenarios: [],
    allInitiatives: []
  }),
  getters: {
    settings: (state) => {
      const core = useCoreStore()
      return core.setting('commute')
    }
  },
  actions: {
    resetState () {
      this.$reset()
    },
    getAllGHGI (data) {
      let toCompute = false
      return carbonService
        .getAllGHGIAdmin(data)
        .then((allGHGI) => {
          this.setAllGHGI(allGHGI, toCompute)
          return allGHGI
        })
        .catch(error => {
          throw error
        })
    },
    setAllGHGI (allGHGI, toCompute) {
      this.allGHGI = []
      for (let ghgi of allGHGI) {
        let cghgi = GHGI.createFromObj(ghgi)
        if (toCompute) {
          cghgi.compute(this.settings)
        }
        this.allGHGI.unshift(cghgi)
      }
    },
    getScenarios (data) {
      return scenarioService.getScenariosAdmin(data)
        .then(scenarios => {
          this.scenarios = []
          for (let scenario of scenarios) {
            let csenario = Scenario.createFromObj(scenario)
            this.scenarios.unshift(csenario)
          }
          return scenarios
        })
        .catch(error => {
          throw error
        })
    },
    unsubmitData (data) {
      let toCompute = false
      return carbonService
        .unsubmitData(data)
        .then((allGHGI) => {
          this.setAllGHGI(allGHGI, toCompute)
          return allGHGI
        })
        .catch(error => {
          throw error
        })
    },
    getAllInitiatives () {
      return transitionService.getInitiativesAdmin()
        .then(allInitiatives => {
          this.allInitiatives = allInitiatives
          return allInitiatives
        })
        .catch(error => {
          throw error
        })
    },
    valdidateInitiative (initiative) {
      return transitionService.validateInitiative(initiative)
        .then(allInitiatives => {
          this.allInitiatives = allInitiatives
          return allInitiatives
        })
        .catch(error => {
          throw error
        })
    }
  }
})
