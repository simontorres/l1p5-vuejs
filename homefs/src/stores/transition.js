import { defineStore } from 'pinia'
import transitionService from '@/services/transitionService'

export const useTransitionStore = defineStore('transition', {
  state: () => ({
    initiatives: []
  }),
  actions: {
    resetState () {
      this.$reset()
    },
    getInitiatives () {
      return transitionService.getInitiatives()
        .then(initiatives => {
          this.initiatives = initiatives
        })
        .catch(error => {
          throw error.response.data
        })
    },
    addInitiative (initiative) {
      transitionService.addInitiative(initiative)
        .then(() => {
          state.initiative.push(initiative)
        })
    }
  }
})
