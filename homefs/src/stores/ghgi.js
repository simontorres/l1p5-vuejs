/**********************************************************************************************************
* Author :
*   Jerome Mariette, INRAE, UR875 Mathématiques et Informatique Appliquées Toulouse, F-31326 Castanet-Tolosan, France
*
* Copyright (C) 2020
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
***********************************************************************************************************/

import carbonService from '@/services/carbonService'
import GHGI from '@/models/carbon/GHGI.js'
import Modules from '@/models/Modules.js'

import { defineStore } from 'pinia'
import { useCoreStore } from './core'
import { useAdminStore } from './admin'

export const useGHGIStore = defineStore('ghgi', {
  state: () => ({
    item: new GHGI(),
    computed: false
  }),
  getters: {
    settings: (state) => {
      const core = useCoreStore()
      return core.setting('commute')
    },
    modules: (state) => {
      const core = useCoreStore()
      return core.setting('global', 'ACTIVE_MODULES').value
    },
    items: (state) => (module) => { return state.item[module].toArray() },
    collection: (state) => (module) => { return state.item[module] },
    hasItems: (state) => (module) => {
      return state.item[module].length > 0
    },
    otherItems: (state) => (module) => {
      return state.item[module].filter(item => item.isOther())
    },
    validItems: (state) => (module) => {
      return state.item[module].filter(item => item.isValid())
    },
    incompleteItems: (state) => (module) => {
      return state.item[module].filter(item => item.isIncomplete())
    },
    invalidItems: (state) => (module) => {
      return state.item[module].filter(item => item.isInvalid())
    },
    countItems: (state) => (data) => {
      return state.item[data.module].countItems(
        data.testFuncion,
        data.param
      )
    },
    isClonedSurvey: (state) => state.item.surveyCloneYear !== null,
    nMember: (state) => {
      return state.item.nResearcher +
        state.item.nProfessor +
        state.item.nEngineer +
        state.item.nStudent
    }
  },
  actions: {
    resetState () {
      this.$reset()
    },
    addItem (data) {
      this.item[data.module].add(data.item, data.param)
    },
    updateItem (data) {
      this.item[data.module].update(data.item)
    },
    updateItems (data) {
      this.item[data.module].reset()
      for (let item of data.items) {
        this.item[data.module].add(item)
      }
    },
    deleteItem (data) {
      this.item[data.module].delete(data.item)
    },
    deleteSource (data) {
      if (data.source) {
        this.item[data.module].deleteSource(data.source)
      } else {
        this.item[data.module].reset()
      }
    },
    deleteInvalidItems (module) {
      this.item[module].deleteInvalid()
    },
    sortItems (module) {
      this.item[module].sortItems()
    },
    saveItems (module) {
      let func = null
      if (module === Modules.BUILDINGS) {
        func = carbonService.saveBuildings
      } else if (module === Modules.VEHICLES) {
        func = carbonService.saveVehicles
      } else if (module === Modules.PURCHASES) {
        func = carbonService.savePurchases
      } else if (module === Modules.DEVICES) {
        func = carbonService.saveComputerDevices
      } else if (module === Modules.TRAVELS) {
        func = carbonService.saveTravels
      } else if (module === Modules.COMMUTES) {
        func = carbonService.saveCommutes
      }
      this.compute(true)
      let allData = {
        [module]: this.collection(module).reduceItems().map(obj => obj.toDatabase()),
        'ghgi_id': this.item.id,
        synthesis: this.item.synthesis
      }
      return func(allData)
        .then(data => {
          this.updateItems({
            module: module,
            items: data
          })
          this.forceComputation()
          return data
        })
        .catch(error => {
          throw error
        })
    },
    compute (force = false) {
      if (force) { this.forceComputation() }
      if (!this.computed) {
        this.item.compute(this.settings, this.item.year)
        this.computed = true
      }
    },
    saveSurveyMessage (surveyMessage) {
      let allData = {
        'surveyMessage': surveyMessage,
        'ghgi_id': this.item.id
      }
      return carbonService.saveSurveyMessage(allData)
        .then(data => {
          this.item.surveyMessage = surveyMessage
          return data
        })
        .catch(error => {
          throw error
        })
    },
    updateGHGI (ghgi) {
      this.item.id = ghgi.id
      this.item.uuid = ghgi.uuid
      this.item.laboratory = ghgi.laboratory
      this.item.buildingsSubmitted = ghgi.buildingsSubmitted
      this.item.commutesSubmitted = ghgi.commutesSubmitted
      this.item.devicesSubmitted = ghgi.devicesSubmitted
      this.item.travelsSubmitted = ghgi.travelsSubmitted
      this.item.vehiclesSubmitted = ghgi.vehiclesSubmitted
      this.item.purchasesSubmitted = ghgi.purchasesSubmitted
      this.item.surveyActive = ghgi.surveyActive
      this.item.surveyCloneYear = ghgi.surveyCloneYear
      this.item.year = ghgi.year
      this.item.description = ghgi.description
      this.item.surveyMessage = ghgi.surveyMessage
      this.item.nResearcher = ghgi.nResearcher
      this.item.nProfessor = ghgi.nProfessor
      this.item.nEngineer = ghgi.nEngineer
      this.item.nStudent = ghgi.nStudent
      this.item.budget = ghgi.budget
    },
    set (id) {
      return carbonService.getGHGIConsumptions({ 'ghgi_id': id })
        .then(data => {
          this.updateGHGI(data['ghgi'])
          const admin = useAdminStore()
          admin.updateLaboratory(data['ghgi'].laboratory)
          for (let module of Modules.getModules(
            false,
            this.modules
          )) {
            this.updateItems({
              module: module,
              items: data[module]
            })
          }
          this.compute(true)
          return data
        })
        .catch(error => {
          throw error
        })
    },
    setFromUUID (uuid) {
      return carbonService.getGHGIConsumptionsByUUID({ 'uuid': uuid })
        .then(data => {
          this.updateGHGI(data['ghgi'])
          const admin = useAdminStore()
          admin.updateLaboratory(data['ghgi'].laboratory)
          for (let module of Modules.getModules(
            false,
            this.modules
          )) {
            this.updateItems({
              module: module,
              items: data[module]
            })
          }
          this.compute(true)
          return data
        })
        .catch(error => {
          throw error
        })
    },
    saveGHGI (ghgi) {
      return carbonService.saveGHGI(ghgi)
        .then(data => {
          this.updateGHGI(JSON.parse(JSON.stringify(data)))
          const admin = useAdminStore()
          admin.getAllGHGI()
          return data
        })
        .catch(error => {
          throw error
        })
    },
    submitData (data) {
      return carbonService.submitData({
        'ghgi_id': this.item.id,
        'module': data.module,
        'synthesis': data.synthesis
      })
        .then(data => {
          this.updateGHGI(JSON.parse(JSON.stringify(data)))
          return data
        })
        .catch(error => {
          throw error
        })
    },
    resetGHGI () {
      this.$reset()
    },
    forceComputation () {
      this.computed = false
    },
    cloneSurvey (surveyCloneYear) {
      let allData = {
        'surveyCloneYear': surveyCloneYear,
        'ghgi_id': this.item['id']
      }
      return carbonService.cloneSurvey(allData)
        .then(data => {
          this.updateGHGI(JSON.parse(JSON.stringify(data['ghgi'])))
          this.updateItems({
            module: Modules.COMMUTES,
            items: data[Modules.COMMUTES]
          })
          return data
        })
        .catch(error => {
          throw error
        })
    },
    activateGHGISurvey () {
      this.compute(true)
      return carbonService.activateGHGISurvey({
        'ghgi_id': this.item.id,
        'synthesis': this.item.synthesis
      })
        .then(data => {
          this.updateGHGI(JSON.parse(JSON.stringify(data)))
          return data
        })
        .catch(error => {
          throw error
        })
    },
    updateYear (year) {
      this.item.year = year
    },
    updateMessage (surveyMessage) {
      this.item.surveyMessage = surveyMessage
    },
    updateNResearcher (nResearcher) {
      this.item.nResearcher = nResearcher
    },
    updateNProfessor (nProfessor) {
      this.item.nProfessor = nProfessor
    },
    updateNEngineer (nEngineer) {
      this.item.nEngineer = nEngineer
    },
    updateNStudent (nStudent) {
      this.item.nStudent = nStudent
    },
    updateNBudget (budget) {
      this.item.budget = budget
    },
    updateDescription (description) {
      this.item.description = description
    }
  }
})
