import api from '@/services/api'

export default {
  getInitiatives () {
    return api.get(`get_initiatives/`)
      .then(response => response.data)
  },
  getInitiativesAdmin () {
    return api.get(`get_initiatives_admin/`)
      .then(response => response.data)
  },
  validateInitiative (payload) {
    return api.post(`validate_initiative/`, payload)
      .then(response => response.data)
  },
  getInitiative () {
    return api.get(`get_initiative/`)
      .then(response => response.data)
  },
  saveInitiative (payload) {
    return api.post(`save_initiative/`, payload)
      .then(response => response.data)
  }
}
